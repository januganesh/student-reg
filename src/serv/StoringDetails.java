package serv;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.instrument.ClassFileTransformer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StoringDetails
 */
@WebServlet("/StoringDetails")
public class StoringDetails extends HttpServlet {
	static Connection con;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StoringDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	PrintWriter out=response.getWriter();
	response.setContentType("text/html");
	String name=request.getParameter("rname");
	Integer Age=Integer.parseInt(request.getParameter("rage"));
	String mname=request.getParameter("mname");
	String fname=request.getParameter("fname");
	String scname=request.getParameter("rschool");
	Integer smark=Integer.parseInt(request.getParameter("smark"));
	Integer hmark=Integer.parseInt(request.getParameter("hmark"));
	String dept=request.getParameter("dept");
	
	try
	{
		Class.forName("com.mysql.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info","root","admin");
		String query="insert into  details values(?,?,?,?,?,?,?,?)";
	    PreparedStatement preparedStmt = con.prepareStatement(query);
	    preparedStmt.setString(1, name);
	    preparedStmt.setInt(2, Age);
	    preparedStmt.setString(3, mname);
	    preparedStmt.setString(4, fname);
	    preparedStmt.setString(5, scname);
	    preparedStmt.setInt(6, smark);
	    preparedStmt.setInt(7, hmark);
	    preparedStmt.setString(8, dept);
	    preparedStmt.execute();
	    out.print("Student Data Stored Successfully");
	    out.println("<form  action=\"SeeStudent.html\">");
		out.println("<input type =\"submit\" value=\"See Student\">");
		out.println("</form>");
		
		
	}
catch (Exception e) 
	{
	// TODO: handle exception
}
	}

}
