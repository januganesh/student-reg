package serv;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SeeStudent
 */
@WebServlet("/SeeStudent")
public class SeeStudent extends HttpServlet {
	static Connection con;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeeStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("unme");
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info","root","admin");
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery("select * from  details where name='"+name+"'");
			
			while(rs.next())
			{
				out.println("<table border=\"2\">");
				out.println("<tr>");
				out.println("<td>Name</th>");
				out.println("<td>");
				out.println(rs.getString(1));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>Age</th>");
				out.println("<td>");
				out.println(rs.getInt(2));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>Mother Name</th>");
				out.println("<td>");
				out.println(rs.getString(3));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>Father Name</th>");
				out.println("<td>");
				out.println(rs.getString(4));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>School Name</th>");
				out.println("<td>");
				out.println(rs.getString(5));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>SSLC Mark</th>");
				out.println("<td>");
				out.println(rs.getInt(6));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>HSC Mark</th>");
				out.println("<td>");
				out.println(rs.getInt(7));
				out.println("</td>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>Department</th>");
				out.println("<td>");
				out.println(rs.getString(8));
				out.println("</td>");
				out.println("</tr>");
				
				
				out.println("</table>");
				
				
				
				
				
			}
			
		}
		catch (Exception e) 
		{
			
		}
	}

}

