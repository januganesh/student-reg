package serv;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.java2d.StateTracker;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	static Connection con; 
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("rname");
		String password=request.getParameter("rpass");

		boolean flag=false;
		try
		{
		Class.forName("com.mysql.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info","root","admin");
		Statement st=con.createStatement();
		ResultSet rs=st.executeQuery("select * from  logininfo");
		while (rs.next())
		{
			if(name.equals(rs.getString("name"))&&password.equals(rs.getString("password")))
					{
			
			    flag=true;
				
			   response.sendRedirect("studentform.html");
				
					}
		}
		if(flag==false)
		{
			
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Invalid Username or Password');"); 
			out.println("</script>");
			RequestDispatcher rd=request.getRequestDispatcher("index.html");
			rd.include(request, response);
			 
		}
		
	}
		catch (Exception e) 
		{
		
	  }
	}
}

