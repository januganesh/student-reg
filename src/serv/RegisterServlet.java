package serv;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	static Connection con;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	
		PrintWriter out= response.getWriter();
		String name=request.getParameter("rname");
		String password=request.getParameter("rpass");
		try
		{
		Class.forName("com.mysql.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info","root","admin");
		String query="insert into LoginInfo values(?,?)";
		PreparedStatement preparedStmt = con.prepareStatement(query);
		preparedStmt.setString(1, name);
		preparedStmt.setString(2, password);
		preparedStmt.execute();
		response.sendRedirect("studentform.html");
		
		
	}
		catch (Exception e) {
			// TODO: handle exception
		}
	

}
}
